﻿using TimeSlotClassLibrary;

Console.Write("Enter time: ");
string time = Console.ReadLine();

KeyValuePair<int, int> hourMinutes = TimeLibrary.ParseTime(time);
Console.WriteLine($"{hourMinutes.Key} : {hourMinutes.Value}");
Console.ReadKey();

