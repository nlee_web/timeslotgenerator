﻿using System.Text.RegularExpressions;

namespace TimeSlotClassLibrary;
public static class TimeLibrary
{
    private static bool IsTime(string time)
    {
        string pattern = "[0-9]{2}:[0-9]{2}";
        Regex regex = new(pattern);
        bool match = regex.IsMatch(time);
        return match;
    }

    private static string[] SplitTime(string time, string delimiter = ":")
    {
        string[] splitTime = time.Split(delimiter);
        return splitTime;
    }

    public static KeyValuePair<int, int> ParseTime(string time)
    {
        bool isTime = IsTime(time);

        if (!isTime)
        {
            throw new Exception($"Invalid input. {time} is not a valid time.");
        }

        string[] splitTime = SplitTime(time);

        int hour = int.Parse(splitTime[0]);
        int minutes = int.Parse(splitTime[1]);

        KeyValuePair<int, int> results = new(hour, minutes);
        return results;
    }
}

